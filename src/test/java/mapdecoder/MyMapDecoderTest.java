package mapdecoder;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

import mapdecoder.MyMapDecoder.*;

public class MyMapDecoderTest {


    /*Correct String*/
    @Test
    public void MyMapDecoder_String_input() {

        MyMapDecoder mapDecoder = new MyMapDecoder();
        Map<String, String> map = mapDecoder.decode("one=1&two=2");
        assertEquals("1", map.get("one"));
        assertEquals("2", map.get("two"));

    }

    /*String with empty key or valuse*/
    @Test
    public void MyMapDecoder_String_with_empty_key_or_valuse() {

        MyMapDecoder mapDecoder = new MyMapDecoder();
        Map<String, String> map = mapDecoder.decode("=43&67=");
        assertEquals("43", map.get(""));
        assertEquals("", map.get("67"));
    }

    /*Empty String*/
    @Test
    public void MyMapDecoder_String_empty() {

        MyMapDecoder mapDecoder = new MyMapDecoder();
        Map<String, String> map = mapDecoder.decode("");
        assertEquals(true, map.isEmpty());


    }

    /*String is null*/
    @Test
    public void MyMapDecoder_Sting_is_null() {

        MyMapDecoder mapDecoder = new MyMapDecoder();
        Map<String, String> map = mapDecoder.decode(null);
        assertEquals(true, map == null);
    }

    /*String with invalid format*/

    @Test(expected = IllegalArgumentException.class)
    public void MyMapDecoder_String_with_invali_format() {

        MyMapDecoder mapDecoder = new MyMapDecoder();
        Map<String, String> map = mapDecoder.decode("invalid format of string");

    }
}
