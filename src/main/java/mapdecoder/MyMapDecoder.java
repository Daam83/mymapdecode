package mapdecoder;



import java.util.Map;
import java.util.HashMap;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class MyMapDecoder implements MapDecoder {
    @Override
    public Map<String, String> decode(String s) {
        Map<String, String> map = new HashMap<String, String>(); // implements map which are empty
        boolean invalidString = true;  //  variable for check if string have invalid format

        if (s == null) {// Check if string isn't null if yes then return null
            return null;
        } else if (s.isEmpty()) { // Check if given map is empty they return empty map
            return map;
        } else if (!(s.contains("%") || s.contains("="))) {
            throw new IllegalArgumentException("String have invalid format " + s);


        } else {
            String[] pars = s.split("&"); // split "s" string  for table of Strings


            for (int i = 0; i < pars.length; i++) { // loop for each par in "s"
//                for(String r:pars[i].split("=")){
//                    System.out.println(i+" "+r);
//                    System.out.println("---");
//                }

                String[] values = pars[i].split("="); // another split for key and value
//                System.out.println(values.length);
                if (values.length == 1) {
                    map.put(values[0], "");
                }
//                System.out.println(values[1]);
                if (values.length == 2) { // condition for good format
                    map.put(values[0], values[1]); // add for map key and value
                }
            }
            return map;

        }


    }

    public static void main(String[] args) { //  for empiric Test
        String s = "one=1&two=2"; // correct string
        String s1 = "";   // empty string
        String s2 = "=43&poPusty=12&67="; // string with empty key or valuse
        String s3 = "invalid format of string";
        MyMapDecoder decode = new MyMapDecoder();
//        correct of print map
        System.out.println("S: one=1&two=2");
        for (Map.Entry<String, String> i : decode.decode(s).entrySet()) {
            System.out.println(i.getKey());
            System.out.println(i.getValue());
            System.out.println("------");

        }
        /*check if empty String give empty map*/
        System.out.println("S1: empty string");
        System.out.println("If String is empty the map are empty? " + decode.decode(s1).isEmpty());
        System.out.println("------");
        // check for empty key and value
        System.out.println("S2: with empty key or valuse");
        for (Map.Entry<String, String> i : decode.decode(s2).entrySet()) {
            System.out.println(i.getKey());
            System.out.println(i.getValue());
            System.out.println("------");

        }

        //check invalid format of string
        System.out.println("S3: invalid format of string without & and =");
        decode.decode(s3);



    }
}





